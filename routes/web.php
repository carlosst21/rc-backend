<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/menus', 'MenuController@index')->name('menus.index');
Route::post('/menus', 'MenuController@store')->name('menus.store');
Route::get('/menus/edit/{id}', 'MenuController@editView')->name('menus.edit_view');
Route::get('/menus/new', 'MenuController@new')->name('menus.new');
Route::post('/menus/{id}', 'menuController@edit')->name('menus.edit');
Route::delete('/menus/{id}', 'MenuController@destroy')->name('menu.destroy');

Route::get('/submenus/{id}', 'SubMenuController@index')->name('submenus.index');
Route::get('/submenus/new/{id}', 'SubMenuController@new')->name('submenus.new');
Route::post('/submenus/store/{id}', 'SubMenuController@store')->name('submenus.store');
Route::get('/submenus/edit/{id}', 'SubMenuController@editView')->name('submenus.edit_view');
Route::post('/submenus/{id}', 'SubMenuController@edit')->name('submenus.edit');
Route::delete('/submenus/{id}', 'SubMenuController@destroy')->name('submenu.destroy');

Route::get('/files', 'FileController@index')->name('files.index');
Route::post('/files', 'FileController@store')->name('files.store');
Route::get('/files/edit/{id}', 'FileController@editView')->name('files.edit_view');
Route::get('/files/new', 'FileController@new')->name('file.new');
Route::post('/files/{id}', 'FileController@edit')->name('files.edit');
Route::delete('/files/{id}', 'FileController@destroy')->name('file.destroy');

