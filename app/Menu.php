<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = [
        'name', 'order', 
    ];
    
    public function submenus() {
        return $this->hasMany(Submenu::class);
    }

    public function countSubmenu(){
        return $this->submenus()->count();
    }
}
