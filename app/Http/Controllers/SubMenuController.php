<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use App\Menu;
use App\Submenu;

class SubMenuController extends Controller
{
    public function _construct(){
        $this->middleware('auth');
    }

    /* Muestra lista de submenu de un menu
     * @param Request $request
     * @param id menu $id
     * $return Response
     */
    public function index(Request $request,$id) {
        $menu = Menu::find($id);
        $submenus = Submenu::where('menu_id',$id)->paginate(Config::get('app.pags'));
        return view('submenus.index',['menu' => $menu, 'submenus' => $submenus]);
    }

    /* Crea un nuevo submenu
     * @param Request $request
     * $return Response
     */
    public function store(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required|max:50',
            'route' => 'required|max:100',
        ]);
        /* $permission = new Permission;
        $permission->name = $request->name;
        $permission->guard_name = 'web';
        $permission->save(); */
        $submenu = new Submenu;
        $submenu->name = $request->name;
        $submenu->route = $request->route;
        $menu = Menu::find($id);
        $submenu->menu_id = $menu->id;
        $submenu->save();
        return redirect('/menus');
    }

    /* Elimina el submenu
     * @param Request $request
     * $return Response
     */
    public function destroy($id){
        $submenu = Submenu::find($id);
        if(empty($submenu)) {
            return redirect('/menus');
        }
        $menu = $submenu->menu;
        $submenu->delete();
        return view('submenus.index',['menu' => $menu]); //redirect('/submenus');
    }

    public function new($id){
        $submenu = null;
        $menu = Menu::find($id);
        return view('submenus.edit',['submenu' => $submenu, 'menu' => $menu]);
    }

    /* Edita el Submenu
     * @param Request $request
     * @param id $id
     * $return Response
     */
    public function edit(Request $request, $id){
        $this->validate($request, [
            'name' => 'required|max:50',
            'route' => 'required|max:100',
        ]);
        $submenu = Submenu::find($id);
        if(empty($submenu)) {
            return redirect('/menus');
        }
        /* $permission = Permission::where('name',$submenu->name)->get();
        $permission->name = $request->name;
        $permi1 = P ermission::where('name',$submenu->name)->get();
        */
        $submenu->name = $request->name;
        $submenu->route = $request->route;
        $submenu->save();
        $menu = $submenu->menu;
        $submenus = Submenu::where('menu_id',$menu->id)->paginate(Config::get('app.pags'));
        return view('submenus.index',['menu' => $menu, 'submenus' => $submenus]);
    }

    /* Direcciona a editar el menu
     * @param Request $request
     * $return Response
     */
    public function editView($id){
        $submenu = Submenu::find($id);
        if(empty($submenu)) {
            return redirect('/submenus');
        }
        return view('submenus.edit',['submenu' => $submenu, 'menu' => $submenu->menu]);
    }

}
