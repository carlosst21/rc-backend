<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use App\Menu;

class MenuController extends Controller
{
    public function _construct(){
        $this->middleware('auth');
    }

    /* Muestra lista de menu
     * @param Request $request
     * $return Response
     */
    public function index(Request $request) {
        $menus = Menu::orderBy('created_at', 'desc')->paginate(Config::get('app.pags'));
        return view('menus.index',['menus' => $menus]);
    }

    /* Crea un nuevo menu
     * @param Request $request
     * $return Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required|max:50',
            'order' => 'required',
        ]);
        $menu = new Menu;
        $menu->name = $request->name;
        $menu->order = $request->order;
        $menu->save();
        return redirect('/menus');
    }

    /* Elimina el menu
     * @param Request $request
     * $return Response
     */
    public function destroy($id){
        $menu = Menu::find($id);
        if(empty($menu)) {
            return redirect('/menus');
        }
        $menu->delete();
        return redirect('/menus');
    }

    public function new(){
        $menu = null;
        return view('menus.edit',['menu' => $menu]);
    }

    /* Edita el menu
     * @param Request $request
     * @param id $id
     * $return Response
     */
    public function edit(Request $request, $id){
        $this->validate($request, [
            'name' => 'required|max:50',
            'order' => 'required',
        ]);
        $menu = Menu::find($id);
        if(empty($menu)) {
            return redirect('/menus');
        }
        $menu->name = $request->name;
        $menu->order - $request->order;
        $menu->save();
        return redirect('/menus');
    }

    /* Direcciona a editar el menu
     * @param Request $request
     * $return Response
     */
    public function editView($id){
        $menu = Menu::find($id);
        if(empty($menu)) {
            return redirect('/menus');
        }
        return view('menus.edit',['menu' => $menu]);
    }


}
