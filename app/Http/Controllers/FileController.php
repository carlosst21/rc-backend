<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

use Illuminate\Http\Request;

use App\File;
use App\User;

class FileController extends Controller
{
    public function _construct(){
        $this->middleware('auth');
    }

    /* Muestra lista de archivos
     * @param Request $request
     * $return Response
     */
    public function index(Request $request) {
        if (Auth::user()->isAdmin()) {
            $files = File::orderBy('title', 'asc')->paginate(Config::get('app.pags'));
        } else {
            $files = File::where('user_id',Auth::user()->id)->orderBy('title', 'asc')
            ->paginate(Config::get('app.pags'));
        }
        return view('files.index',['files' => $files]);
    }

    /* Crea un nuevo archivo
     * @param Request $request
     * $return Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'title' => 'required|max:150',
            'file' => 'required|max:200',
            'user_id'=> 'required',
        ]);
        
        if (Auth::user()->isAdmin()) {
            $user = User::find($request->user_id);
        } else {
            $user=Auth::user();
        }
        $arch = $request->file('file');
        $nombre = $arch->getClientOriginalName();
        \Storage::disk('local')->put($nombre,  \File::get($arch));
        $file = new File;
        $file->title = $request->title;
        $file->file = $nombre;
        $file->user_id = $user->id;
        $file->save();
        if (Auth::user()->isAdmin()) {
            $files = File::orderBy('title', 'asc')->paginate(Config::get('app.pags'));
        } else {
            $files = File::where('user_id',Auth::user()->id)->orderBy('title', 'asc')
            ->paginate(Config::get('app.pags'));
        }
        return view('files.index',['files' => $files]);
    }

    /* Elimina el archivo
     * @param Request $request
     * $return Response
     */
    public function destroy($id){
        $file = File::find($id);

        if(empty($file)) {
            return redirect('/files');
        }
        \Storage::delete($file->file);
        $file->delete();
        if (Auth::user()->isAdmin()) {
            $files = File::orderBy('title', 'asc')->paginate(Config::get('app.pags'));
        } else {
            $files = File::where('user_id',Auth::user()->id)->orderBy('title', 'asc')
            ->paginate(Config::get('app.pags'));
        }
        return view('files.index',['files' => $files]);
       
    }

    public function new(){
        $file = null;
        if (Auth::user()->isAdmin()) {
            $users = User::orderBy('name','asc')->pluck('name', 'id');
        }
        else {
            $users = User::where('id',Auth::user()->id)->orderBy('name','asc')->pluck('name', 'id');
        }

        return view('files.edit',['file' => $file, 'users' => $users]);
    }

    /* Edita el archivo
     * @param Request $request
     * @param id $id
     * $return Response
     */
    public function edit(Request $request, $id){
       
        $this->validate($request, [
            'title' => 'required|max:150',
        ]);
        
        $file = File::find($id);
        if(empty($file)) {
            return redirect('/files');
        }
        $file->title = $request->title;
        $file->save();
        return redirect('/files');
    }

    /* Direcciona a editar el archivo
     * @param Request $request
     * $return Response
     */
    public function editView($id){
        $file = File::find($id);
        if(empty($file)) {
            return redirect('/files');
        }
        if (Auth::user()->isAdmin()) {
            $users = User::orderBy('name','asc')->pluck('name', 'id');
        }
        else {
            $users = User::where('id',Auth::user()->id)->orderBy('name','asc')->pluck('name', 'id');
        }
        return view('files.edit',['file' => $file, 'users' => $users]);
    }
}
