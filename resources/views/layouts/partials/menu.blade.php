<?php
use App\Menu;
?>

<nav class="navbar navbar-expand-lg navbar-light bg-light">

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
        aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            @empty(!Auth::user())
                @foreach (Menu::all() as $menu)
                    @if ($menu->countSubMenu() > 0)
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="cis-folder"></i>
                                {{ $menu->name }}</a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" style="width: 300px;">

                                @foreach ($menu->submenus as $submenu)

                                    <a class="dropdown-item {{ !Auth::user()->isPermited($submenu->name) ? 'disabled' : '' }}"
                                        href="{{ route($submenu->route) }}">{{ $submenu->name }}</a>
                                @endforeach
                            </div>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link disabled " href="#">{{ $menu->name }}</a>
                        </li>
                    @endif
                @endforeach
            @endempty
            <!--
        <li class="nav-item active">
            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Features</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Pricing</a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown link
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <a class="dropdown-item" href="#">Something else here</a>
            </div>
        </li> 
    -->
        </ul>
    </div>
</nav>
