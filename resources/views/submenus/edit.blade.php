@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        @if (!empty($submenu))
                            Editar Submenu {{ $submenu->name }}
                        @else
                            Nuevo SubMenu de Menu {{ $menu->name }}
                        @endif
                    </div>

                    <div class="card-body">
                        <form method="POST"
                            action="{{ !empty($submenu) ? route('submenus.edit', [$submenu->id]) : route('submenus.store', [$menu->id]) }}">
                            @csrf

                            <div class="form-group row">
                                <input id="submenu-menuid" type="hidden" value="{{ $menu->id }}" class="form-control"
                                    name="menuid">
                                <label for="submenu-name" class="col-md-4 col-form-label text-md-right">Nombre:</label>
                                <div class="col-md-6">
                                    <input id="submenu-name" type="text"
                                        class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"
                                        value="{{ !empty($submenu) ? $submenu->name : old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="submenu-route" class="col-md-4 col-form-label text-md-right">Ruta:</label>
                                <div class="col-md-6">
                                    <input id="submenu-route" type="text"
                                        class="form-control{{ $errors->has('route') ? ' is-invalid' : '' }}" name="route"
                                        value="{{ !empty($submenu) ? $submenu->route : old('route') }}" required
                                        autofocus>

                                    @if ($errors->has('route'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('route') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Guardar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
