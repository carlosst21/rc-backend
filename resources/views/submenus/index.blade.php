@extends('layouts.app')

@section('content')
    <div class="container">
        <section class="row justify-content-center">
            <div class="col-md-8">
                @if (!empty($menu))
                    <h4>Submenus del Menu {{ $menu->name }}</h4>
                    <table class="table">
                        <thead class="table-dark">
                            <tr>
                                <th scope="col">Nombre</th>
                                <th scope="col">Ruta</th>
                                <th scope="col">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($submenus as $submenu)
                                <tr>
                                    <td>{{ $submenu->name }}</td>
                                    <td>{{ $submenu->route }}</td>
                                    <td style="display: flex">
                                        <a class="btn btn-info" href="{{ route('submenus.edit_view', [$submenu->id]) }}"
                                            style="margin-right: 5px;">Editar</a>

                                        <form action="{{ route('submenu.destroy', [$submenu->id]) }}" method="POST">
                                            @csrf
                                            {{ method_field('DELETE') }}
                                            @if (Auth::user()->isAdmin())
                                                <button type="submit" class="btn btn-danger">Eliminar</button>
                                            @else

                                            @endif
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @else
                    <h3>No hay datos</h3>
                @endif
            </div>
            <div class="col-md-8">
                {!! $submenus->links() !!}
                <div class="container text-right">
                    <a class="btn btn-info" href="{{ route('submenus.new', [$menu->id]) }}">Nuevo</a>
                </div>
            </div>
        </section>

    </div>
@endsection
