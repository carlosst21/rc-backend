@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        @if (!empty($menu))
                            Editar Menu
                        @else
                            Nuevo Menu
                        @endif
                    </div>

                    <div class="card-body">
                        <form method="POST"
                            action="{{ !empty($menu) ? route('menus.edit', [$menu->id]) : route('menus.store') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="menu-name" class="col-md-4 col-form-label text-md-right">Nombre:</label>

                                <div class="col-md-6">
                                    <input id="menu-name" type="text"
                                        class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"
                                        value="{{ !empty($menu) ? $menu->name : old('name') }}" required autofocus>
                                        
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="menu-order" class="col-md-4 col-form-label text-md-right">Orden:</label>
                                <div class="col-md-6">
                                    <input id="menu-order" type="text"
                                        class="form-control{{ $errors->has('order') ? ' is-invalid' : '' }}" name="order"
                                        value="{{ !empty($menu) ? $menu->order : old('order') }}" required autofocus>

                                    @if ($errors->has('order'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('order') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Guardar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
