@extends('layouts.app')

@section('content')
    <div class="container">
        <section class="row justify-content-center">
            <div class="col-md-8">
                @if (!empty($menus))
                    <h4>Lista de Menu</h4>
                    <table class="table">
                        <thead class="table-dark">
                            <tr>
                                <th scope="col">Nombre</th>
                                <th scope="col">Orden</th>
                                <th scope="col">Submenus</th>
                                <th scope="col">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($menus as $menu)
                                <tr>
                                    <td>{{ $menu->name }}</td>
                                    <td>{{ $menu->order }}</td>
                                    <td>{{ $menu->countSubMenu() }}</td>
                                    <td style="display: flex">
                                        <a class="btn btn-info" href="{{ route('menus.edit_view', [$menu->id]) }}"
                                            style="margin-right: 5px;">Editar</a>

                                        <a class="btn btn-success" href="{{ route('submenus.index', [$menu->id]) }}"
                                            style="margin-right: 5px;">Submenus</a>

                                        <form action="{{ route('menu.destroy', [$menu->id]) }}" method="POST">
                                            @csrf
                                            {{ method_field('DELETE') }}
                                            @if (Auth::user()->isAdmin())
                                                <button type="submit" class="btn btn-danger">Eliminar</button>
                                            @else

                                            @endif
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @else
                    <h3>No hay datos</h3>
                @endif
            </div>
            <div class="col-md-8">
                {!! $menus->links() !!}
                <div class="container text-right">
                    <a class="btn btn-info" href="{{ route('menus.new') }}">Nuevo</a>
                </div>
            </div>
        </section>
    </div>
@endsection
