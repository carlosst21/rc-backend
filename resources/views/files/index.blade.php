@extends('layouts.app')

@section('content')
    <div class="container">
        <section class="row justify-content-center">
            <div class="col-md-8">
                @if (!empty($files))
                    <h4>Lista de archivos</h4>
                    <table class="table">
                        <thead class="table-dark">
                            <tr>
                                <th scope="col">Titulo</th>
                                <th scope="col">Archivo</th>
                                @if (Auth::user()->isAdmin())
                                    <th scope="col">Usuario</th>
                                @endif
                                <th scope="col">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($files as $file)
                                <tr>
                                    <td>{{ $file->title }}</td>
                                    <td><a href="{{ route('home') }}/Storage/{{ $file->file }}">{{ $file->file }}</a>
                                    </td>
                                    @if (Auth::user()->isAdmin())
                                        <td>{{ $file->user->name }}</td>
                                    @endif
                                    <td style="display: flex">
                                        <a class="btn btn-info" href="{{ route('files.edit_view', [$file->id]) }}"
                                            style="margin-right: 5px;">Editar</a>

                                        <form action="{{ route('file.destroy', [$file->id]) }}" method="POST">
                                            @csrf
                                            {{ method_field('DELETE') }}
                                            @if (Auth::user()->isAdmin())
                                                <button type="submit" class="btn btn-danger">Eliminar</button>
                                            @else

                                            @endif
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @else
                    <h3>No hay datos</h3>
                @endif
            </div>
            <div class="col-md-8">
                {!! $files->links() !!}
                <div class="container text-right">
                    <a class="btn btn-info" href="{{ route('file.new') }}">Nuevo</a>
                </div>
            </div>
        </section>
    </div>
@endsection
