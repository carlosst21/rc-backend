@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        @if (!empty($file))
                            Editar Archivo
                        @else
                            Nuevo Archivo
                        @endif
                    </div>

                    <div class="card-body">
                        <form method="POST" accept-charset="UTF-8" enctype="multipart/form-data"
                            action="{{ !empty($file) ? route('files.edit', [$file->id]) : route('files.store') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="file-title" class="col-md-4 col-form-label text-md-right">Titulo:</label>

                                <div class="col-md-6">
                                    <input id="file-title" type="text"
                                        class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title"
                                        value="{{ !empty($file) ? $file->title : old('title') }}" required autofocus>

                                    @if ($errors->has('title'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="file-file" class="col-md-4 col-form-label text-md-right">Archivo:</label>
                                <div class="col-md-6">

                                    <input id="file-file" type="{{ empty($file) ? 'file' : 'text' }}"
                                        {{ !empty($file) ? 'disabled' : '' }} accept=".doc,.docx,.pdf,.xml"
                                        class="form-control{{ $errors->has('file') ? ' is-invalid' : '' }}" name="file"
                                        value="{{ !empty($file) ? $file->file : old('file') }}" required autofocus>

                                    @if ($errors->has('file'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('file') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="file-user_id" class="col-md-4 col-form-label text-md-right">Usuario:</label>
                                <div class="col-md-6">

                                    <select class="form-control" {{ !empty($file) ? 'disabled' : '' }} name="user_id">

                                        @foreach ($users as $key => $value)
                                            <option value="{{ $key }}"
                                                {{ !empty($file) ? ($file->user_id === $key ? 'selected' : '') : '' }}>
                                                {{ $value }}
                                            </option>
                                        @endforeach
                                    </select>

                                    @if ($errors->has('user_id'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('user_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Guardar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
