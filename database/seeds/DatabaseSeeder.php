<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        
        $this->call(UserSeeder::class);
        $this->call(RolesSeeder::class);
        $this->call(ModelxRolesSeeder::class);
        $this->call(MenuSeeder::class);
        $this->call(SubmenuSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(ModelxPermissionSeeder::class);
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
    }
}
