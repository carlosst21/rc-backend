<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::Table('users')->truncate();
        // Insertar datos
        DB::table('users')->insert([
            'name' => 'Administrator',
            'email' => 'admin@contoso.com',
            'password' => bcrypt('12345678')
        ]);
        DB::table('users')->insert([
            'name' => 'user',
            'email' => 'user@contoso.com',
            'password' => bcrypt('12345678')
        ]);
    }
}
