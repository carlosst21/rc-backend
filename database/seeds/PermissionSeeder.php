<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::Table('permissions')->truncate();
        // Insertar datos
        DB::table('permissions')->insert([
            'name' => 'Ejecutivos',
            'guard_name' => 'web',
        ]);
        DB::table('permissions')->insert([
            'name' => 'Imagenes',
            'guard_name' => 'web',
        ]);
        DB::table('permissions')->insert([
            'name' => 'SGR',
            'guard_name' => 'web',
        ]);
        DB::table('permissions')->insert([
            'name' => 'IVA',
            'guard_name' => 'web',
        ]);
        DB::table('permissions')->insert([
            'name' => 'Paises',
            'guard_name' => 'web',
        ]);
        DB::table('permissions')->insert([
            'name' => 'Monedas',
            'guard_name' => 'web',
        ]);
        DB::table('permissions')->insert([
            'name' => 'Uso Plataforma',
            'guard_name' => 'web',
        ]);
        DB::table('permissions')->insert([
            'name' => 'Notaria',
            'guard_name' => 'web',
        ]);
        DB::table('permissions')->insert([
            'name' => 'Submenu',
            'guard_name' => 'web',
        ]);
        DB::table('permissions')->insert([
            'name' => 'SubMenu Actions',
            'guard_name' => 'web',
        ]);
        DB::table('permissions')->insert([
            'name' => 'Carga Masiva Proyectos',
            'guard_name' => 'web',
        ]);
        DB::table('permissions')->insert([
            'name' => 'Archivos',
            'guard_name' => 'web',
        ]);
    }
}
