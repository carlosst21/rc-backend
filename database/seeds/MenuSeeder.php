<?php

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::Table('menus')->truncate();
        // Insertar datos
        DB::table('menus')->insert([
            'name' => 'Archivo',
            'order' => 1,
        ]);
        DB::table('menus')->insert([
            'name' => 'Configuracion',
            'order' => 2,
        ]);
        DB::table('menus')->insert([
            'name' => 'Acerca de',
            'order' => 3,
        ]);
    }
}
