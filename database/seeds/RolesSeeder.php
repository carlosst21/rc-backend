<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::Table('roles')->truncate();
        // Insertar datos
        DB::table('roles')->insert([
            'name' => 'Administrator',
            'guard_name' => 'web',
        ]);
        DB::table('roles')->insert([
            'name' => 'User',
            'guard_name' => 'web',
        ]);
    }
}
