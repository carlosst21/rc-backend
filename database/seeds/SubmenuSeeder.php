<?php

use Illuminate\Database\Seeder;

class SubmenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::Table('submenus')->truncate();
        // Insertar datos
        DB::table('submenus')->insert([
            'name' => 'Ejecutivos',
            'route' => 'home',
            'menu_id' => 2,
        ]);
        DB::table('submenus')->insert([
            'name' => 'Imagenes',
            'route' => 'home',
            'menu_id' => 2,
        ]);
        DB::table('submenus')->insert([
            'name' => 'SGR',
            'route' => 'home',
            'menu_id' => 2,
        ]);
        DB::table('submenus')->insert([
            'name' => 'IVA',
            'route' => 'home',
            'menu_id' => 2,
        ]);
        DB::table('submenus')->insert([
            'name' => 'Paises',
            'route' => 'home',
            'menu_id' => 2,
        ]);
        DB::table('submenus')->insert([
            'name' => 'Monedas',
            'route' => 'home',
            'menu_id' => 2,
        ]);
        DB::table('submenus')->insert([
            'name' => 'Uso Plataforma',
            'route' => 'home',
            'menu_id' => 2,
        ]);
        DB::table('submenus')->insert([
            'name' => 'Notaria',
            'route' => 'home',
            'menu_id' => 2,
        ]);
        DB::table('submenus')->insert([
            'name' => 'Submenu',
            'route' => 'menus.index',
            'menu_id' => 2,
        ]);
        DB::table('submenus')->insert([
            'name' => 'SubMenu Actions',
            'route' => 'home',
            'menu_id' => 2,
        ]);
        DB::table('submenus')->insert([
            'name' => 'Carga Masiva Proyectos',
            'route' => 'home',
            'menu_id' => 2,
        ]);
        DB::table('submenus')->insert([
            'name' => 'Archivos',
            'route' => 'home',
            'menu_id' => 1,
        ]);
    }
}
