El presente proyecto se realizó como evaluacion del uso de Laravel y tiene en consideración los siguientes puntos.

El proyecto contiene los migrations necesarios para generar las tablas necesarias en la base de datos de modo que
con solo configurar la conexion a la base de datos se tendrá la estructura necesaria para su ejecución.

El proyecto tambien tiene los Seeders para la carga inicial de datos para la generación del menú. El mismo será
como sigue:
Archivo
|----- Archivos ( Accede a CRUD de Archivos - accesible por usuarios Administrator y user)
Configuracion (solo accesible por usuario Administrator)
|----- Ejecutivos (sin funcion)
|----- Imagenes (sin funcion)
|----- SGR (sin funcion)
|----- IVA (sin funcion)
|----- Paises (sin funcion)
|----- Monedas (sin funcion)
|----- Uso Plataforma (sin funcion)
|----- Notaria (sin funcion)
|----- Submenu (Accede al CRUD de menú y desde ahi a Submenu)
|----- SubMenu Actions (sin funcion)
|----- Carga Masiva Proyectos (sin funcion)
Acerca de (desabilitado por no tener submenú)

Todo menú que se cree si no tiene submenú estará desabilitado (se puede hacer que no esté presente cuando se genera el menú)
Todo submenú que se cree tiene que tener una ruta válida dentro del proyecto, sino se generará un error por no estar declarado
en el archivo /routes/web.php

El proyecto utiliza la validación de sesión provista por Laravel (Authentication) y se instaló Spatie/laravel-pemission, lo
cual agrega la posibilidad de uso de roles y verificación de permisos del usuario.
El uso de roles se implentó aquí solamente para definir si el usuario es Administrator o user.
El uso de verificación de permisos del usuario se implementó para dar acceso al menú a los usuarios de modo que con la
carga inicial de Seeders se cargó como permission name los mismos nombres que los nombres de los submenú, de modo de usar
estos nombres para validad si el usuario tiene acceso al menú. En caso de no tener acceso el submenú se encontrará desabilitado,
pero podría hacerse que no este presente al momento de generarlo.
También, con la carga inicial se generaron lo siguientes usuarios:

Administrator: username admin@contoso.com con clave: 12345678
user: username user@contoso.com con clave: 12345678

Para validar el acceso de que un usuario tenga que estar autenticado se utilizó en el controller de ese modelo la validación
con el middleware de autenticación Auth.

en cuanto a la funcionalidad de gestión de archivos DOC, PDF y XML, que se encuentra disponible desde el punto de menú
Archivo-> Archivos, el mismo mostrará los archivos que pertenezcan al usuario logueado, salvo que el mismo sea administrador.
En este caso se visualizaran todos los archivos.

La funcionalidad de los CRUD es la siguiente:
Primero se accede a la lista de los registros ya creados (menu, submenu o archivos). La lista esta paginada en una cantidad
de 5 elementos (este valor se puede cambiar en /config/app.php en la linea 6 para la key 'pags').
La paginación se encuentra en la parte inferior izquierda, mientras que en la inferior derecha se encontrará el boton para
agregar un nuevo elemento.
Dentro de la lista se encontrará el boton editar, para modificar el elemento y si se es administrador tambien se encontrará
el botón eliminar.
En referencia a donde se guardan los archivos subidos desde el CRUD archivos, el directorio s /public/storage.
Para el caso especial del CRUD de menú también encontrarán un botón llamado Submenú que permitirá gestionar todos los submenús
del menú correspondiente a la fila en cuestión.

En cuanto al uso de Eloquent, se puede decir que se utilizó los métodos mas comunes para busquedas como find, where, orderBy o
paginate. También el manejo de guardado (creacion o modificación) y eliminación implementados ya en la superclase Model del que
dependen las clases User, Menu, Submenu y File.

Con esto se tiene una idea general de lo realizado, su implementación y funcionamiento. Si se desea tener alguna respuesta en
cuanto a algo puntual, no dude en comunicarse a carlosst21@gmail.com
Muchas gracias
